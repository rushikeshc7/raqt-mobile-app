/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import appNavigator from './src/appNavigator/appNavigator';
import { Provider } from 'react-redux';
import store from './src/redux/store/store';
import * as RNLocalize from 'react-native-localize';
import I18n from 'i18n-js';
// import RNBootSplash from "react-native-bootsplash";

const AppContainer = createAppContainer(appNavigator);
console.disableYellowBox = true;
export default class App extends Component {

  componentDidMount() {
    // RNBootSplash.hide({ duration: 250 });
    this.getLocale()
  }

  getLocale() {
    if (this.locales) {
      if (Array.isArray(this.locales)) {
        return this.locales[0];
      }
    }
    return null;
  }

  async handleLocales() {
    this.locales = RNLocalize.getLocales();
  }
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}