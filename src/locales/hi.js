const hi = {
    phoneNumber: "फ़ोन नंबर",
    login: "लॉग इन करें",
    loginWithFB: "फेसबुक से लॉगिन करें",
    loginWithGoogle: "गूगल से लॉगिन करें",
    loginWithEmail: "ईमेल से लॉगिन करें",
    newHere: "यहाँ नये?",
    signUp: "साइन अप करें",
    skipSignUp: "साइन अप छोड़ें",
    searchForBlood: "रक्त की खोज करें"
}

export default hi;