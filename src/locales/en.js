const en = {
    phoneNumber: "Phone Number",
    login: "Login",
    loginWithFB: "Login with Facebook",
    loginWithGoogle: "Login with Google",
    loginWithEmail: "LOGIN WITH EMAIL",
    newHere: "New Here?",
    signUp: "SIGN UP",
    skipSignUp: "Skip sign up",
    searchForBlood: "SEARCH FOR BLOOD"
}

export default en;