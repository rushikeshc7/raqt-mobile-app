import { StyleSheet, Dimensions } from "react-native";
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: '#ff1654',
        justifyContent: 'center',
    },
    setupText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        color: '#ffffff',
    },
    titleText: {
        fontSize: 22,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: -30,
        color: '#ffffff',
    },
    detailsText: {
        color: 'white',
        fontSize: 15,
        marginTop: 20,
        marginHorizontal: 30,
    },
    whereGiveBloodText: {
        fontSize: 22,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 30,
        marginBottom: 5
    },
    donationCardView: {
        marginTop: 15,
        marginHorizontal: 30,
        bottom: 0
    },
    readFaqBtnView: {
        borderRadius: 19,
        height: 44,
        borderWidth: 1,
        borderColor: 'white',
        marginBottom: 20,
        marginHorizontal: 30,
        elevation: 2,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1,
        marginTop: 40
        //#F69AA8  Google FB
    },
    readFaqText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 9
    },
    goToDashboardText: {
        alignSelf: 'center',
        position: 'absolute',
        bottom: 30,
        fontWeight: 'bold',
        fontSize: 15,
        color: 'white'
    },
    overlaySet: {
        flex: 1,
        flexDirection: 'row'
    },
    icon: {
        color: 'white',
        flex: 1,
        textAlign: 'center',
        fontSize: 25
    },
    sliderCont: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0
    },
    timer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 8
    },
    video: {
        marginHorizontal: 30,
        height: 140,
        backgroundColor: 'black',
        marginTop: 30,
    },
    videoView: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        marginHorizontal: 30,
        marginTop: 30,
        height: 160,
        elevation: 2,
    }
})
export default styles;