import React, { Component } from "react";
import { SafeAreaView, TouchableOpacity, StatusBar, PixelRatio, View, Text, FlatList, ScrollView, TouchableWithoutFeedback, StyleSheet, Dimensions, TouchableNativeFeedback, Slider, Modal } from "react-native";
import styles from './styles';
import VideoPlayer from 'react-native-video-controls';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
const width = Dimensions.get('window').width;
import * as Animatable from 'react-native-animatable';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
class Tutorial extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isSetup: true,
            currentTime: 0,
            duration: 0.1,
            paused: false,
            overlay: false,
            fullscreen: false,
            isVideo: false,
            isModalVisible: false
        }
    }


    componentDidMount() {
        setTimeout(() => {
            this.setState({
                isSetup: false
            })
        }, 1500)
    }


    playVideo() {
        this.setState({
            isVideo: !this.state.isVideo
        })
    }

    fullscreenEnter() {
        this.setState({
            fullscreen: !this.state.fullscreen
        })
    }

    fullscreenExit() {
        this.setState({
            fullscreen: !this.state.fullscreen
        })
    }

    onFAQPressed() {

    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='#ff1654' barStyle='dark-content' />
                {
                    this.state.isSetup
                        ?

                        <Animatable.Text allowFontScaling={false} animation='fadeOut' duration={3000} style={styles.setupText}>You are all set up!</Animatable.Text>

                        :
                        <Animatable.View animation='fadeIn' style={{ flex: 1, justifyContent: 'center' }}>
                            <Text allowFontScaling={false} style={styles.titleText}>First time giving blood?</Text>
                            <Text allowFontScaling={false} style={styles.detailsText}>
                                We understand that donating blood can be quite stressful. That's why we put together a collection of videos and testimonials to help you get a better view of what to expect when giving blood.
                        </Text>

                            {
                                this.state.isVideo === false
                                    ?
                                    <TouchableOpacity style={styles.videoView} onPress={() => this.playVideo()}>
                                        <IconFontAwesome name='play' color='#ff1654' size={45} />
                                    </TouchableOpacity>
                                    :
                                    <View style={this.state.fullscreen ? { top: 0, left: 0, bottom: 0, right: 0, position: 'absolute' } : styles.videoView}>
                                        <VideoPlayer
                                            source={{ uri: 'https://vjs.zencdn.net/v/oceans.mp4' }}
                                            toggleResizeModeOnFullscreen={true}
                                            disableBack
                                            resizeMode='cover'
                                            fullscreen={false}
                                            disableVolume
                                            onEnd={() => this.state.fullscreen === true ?
                                                this.setState({
                                                    fullscreen: false
                                                })
                                                :
                                                this.setState({
                                                    isVideo: false
                                                })
                                            }
                                            fullscreenOrientation="landscape"
                                            onEnterFullscreen={() => this.fullscreenEnter()}
                                            onExitFullscreen={() => this.fullscreenExit()}
                                        />
                                    </View>
                            }
                            {
                                this.state.fullscreen ?
                                    null
                                    :
                                    <View>
                                        <TouchableOpacity onPress={() => this.onFAQPressed()}>
                                            <View style={styles.readFaqBtnView}>
                                                <Text allowFontScaling={false} style={styles.readFaqText}>
                                                    READ FAQS
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                            }
                            {this.state.fullscreen
                                ?
                                null
                                :
                                <Text style={styles.goToDashboardText}>GO TO YOUR DASHBOARD</Text>
                            }
                        </Animatable.View>
                }
            </SafeAreaView>
        )
    }

}

export default Tutorial;