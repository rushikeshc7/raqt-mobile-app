import React, { Component } from 'react';
import { TouchableOpacity, View, Text, TextInput, SafeAreaView, StatusBar, ScrollView, Dimensions } from 'react-native';
const bloodGroupArray = ['A+', 'A-', 'AB+', 'AB-', 'O+', 'O-', 'B+', 'B-'];
import styles from './styles';
import { Slider } from "@miblanchard/react-native-slider";
import IconMaterial from 'react-native-vector-icons/MaterialIcons'
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import { SET_SEEK_BLOOD_GROUP, SET_ADMITTED_HOSPITAL, SET_REQUIRED_DATE_OF_BLOOD, SET_REQUIRED_BLOOD_UNITS } from '../../redux/actionTypes/actionTypes';
import { connect } from 'react-redux';
const width = Dimensions.get('window').width;


const mapDispatchToProps = (dispatch) => ({
    bloodGroupSelect: value =>
        dispatch({ type: SET_SEEK_BLOOD_GROUP, bloodGroupSelected: value }),
    admittedHospital: value =>
        dispatch({ type: SET_ADMITTED_HOSPITAL, admittedHospital: value }),
    requiredDateOfBlood: value =>
        dispatch({ type: SET_REQUIRED_DATE_OF_BLOOD, requiredDateOfBlood: value }),
    requiredUnitsOfBlood: value =>
        dispatch({ type: SET_REQUIRED_BLOOD_UNITS, requiredUnitsOfBlood: value }),
})

class SeekBlood extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bloodGroupSelectedIndex: 0,
            hospitalName: '',
            requiredDateOfBlood: '',
            currentDate: new Date(),
            sliderValue: 1,
        }
    }

    onBloodGroupSelected(item, index) {
        this.setState({
            bloodGroupSelectedIndex: index
        })
        this.props.bloodGroupSelect(item);
    }

    onSeekBloodPressed() {
        this.props.admittedHospital(this.state.hospitalName);
        this.props.requiredDateOfBlood(moment(this.state.requiredDateOfBlood).format('D MMMM'));
        this.props.requiredUnitsOfBlood(this.state.sliderValue);
        this.props.navigation.navigate('seekingBlood');
    }


    render() {
        const left = this.state.sliderValue * (width - 57) / 5 - 45;
        let maxDate = moment(this.state.currentDate).add(7, 'days').format('DD-MMM-YYYY');
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <ScrollView>
                        <Text allowFontScaling={false} style={styles.seekBloodTitleText}>Seek Blood</Text>
                        <Text allowFontScaling={false} style={styles.titleText}>What is your blood group?</Text>
                        <View style={styles.bloodGroupView}>
                            {
                                bloodGroupArray.map((item, index) => {
                                    if (index === this.state.bloodGroupSelectedIndex) {
                                        return (
                                            <TouchableOpacity>
                                                <View style={styles.selcetedCircleView}>
                                                    <Text style={styles.selectedBloodGroupText}>{item}</Text>
                                                </View >
                                            </TouchableOpacity>
                                        )
                                    } else {
                                        return (
                                            <TouchableOpacity onPress={() => this.onBloodGroupSelected(item, index)}>
                                                <View style={styles.circleView}>
                                                    <Text style={styles.bloodGroupText}>{item}</Text>
                                                </View >
                                            </TouchableOpacity>
                                        )

                                    }
                                })
                            }
                        </View>

                        <Text allowFontScaling={false} style={styles.hospitalText}>Where hospital is patient admitted in?</Text>

                        <View style={styles.textInputView}>
                            <TextInput
                                placeholder='Enter Hospital'
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                allowFontScaling={false}
                                keyboardType='default'
                                onSubmitEditing={() => {
                                    this.dateInput.focus()
                                }}
                                onChangeText={(hospitalName) => this.setState({ hospitalName: hospitalName })}
                                value={this.state.hospitalName}
                                returnKeyType='next' />
                        </View>

                        <Text allowFontScaling={false} style={styles.hospitalText}>By when do you need the blood?</Text>

                        <View style={{ marginHorizontal: 40, }}>
                            <DatePicker
                                ref={(dateInput) => { this.dateInput = dateInput }}
                                style={{ height: 43, width: '100%', marginTop: 25, backgroundColor: 'white', borderRadius: 5, borderWidth: 0.7, borderColor: 'black' }}
                                customStyles={{
                                    dateText: { color: '#808080', paddingLeft: 12, alignSelf: 'flex-start', fontSize: 16, },
                                    dateInput: { borderColor: 'white', borderRadius: 5 },
                                    datePicker: { backgroundColor: "#cdcdcd" },
                                    placeholderText: { alignSelf: 'flex-start', fontSize: 16, paddingLeft: 12 },
                                }}
                                date={this.state.requiredDateOfBlood}
                                mode="date"
                                placeholder="Date"
                                allowFontScaling={false}
                                placeholderTextColor="#ccc"
                                showIcon={true}
                                iconComponent={<IconMaterial name='date-range' color='#ff1654' size={30} style={styles.dateIcon} />}
                                format="DD-MMM-YYYY"
                                minDate={this.state.currentDate}
                                maxDate={maxDate}
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                onDateChange={(date) => { this.setState({ requiredDateOfBlood: date }) }}
                            />
                        </View>

                        <Text allowFontScaling={false} style={styles.hospitalText}>How much blood is required?</Text>

                        <Slider
                            value={this.state.sliderValue}
                            containerStyle={styles.slider}
                            minimumTrackTintColor='#ff1654'
                            maximumTrackTintColor='#d8d8d8'
                            minimumValue={1}
                            step={1}
                            maximumValue={5}
                            thumbStyle={styles.sliderThumb}
                            thumbTintColor='white'
                            onValueChange={slider => this.setState({ sliderValue: slider })}
                        />
                        <View style={{ borderRadius: 13, width: 70, left: left, height: 26, backgroundColor: 'white', elevation: 2 }}>
                            <Text style={styles.unitText}>{this.state.sliderValue} Units</Text>
                        </View>

                        <TouchableOpacity onPress={() => this.onSeekBloodPressed()}>
                            <View style={styles.seekBloodBtnView}>
                                <Text allowFontScaling={false} style={styles.seekBloodText}>
                                    SEEK BLOOD
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
            </SafeAreaView>
        )
    }
}
export default connect(undefined, mapDispatchToProps)(SeekBlood);