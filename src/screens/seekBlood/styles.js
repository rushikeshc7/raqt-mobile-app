import { StyleSheet, Dimensions } from "react-native";
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainView: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginHorizontal: 8,
        marginBottom: 10,
        height: '98%',
        elevation: -1,
        justifyContent: 'center'
    },
    seekBloodTitleText: {
        fontSize: 25,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 50
    },
    titleText: {
        fontSize: 22,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 10
    },
    bloodGroupView: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        marginHorizontal: 25,
        width: width * 0.75,
        justifyContent: 'center',
        marginTop: 10
    },
    selcetedCircleView: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        backgroundColor: '#ff1654',
        paddingVertical: 15,
        marginHorizontal: 5,
        marginTop: 10
    },
    selectedBloodGroupText: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    circleView: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        backgroundColor: 'white',
        paddingVertical: 15,
        marginHorizontal: 5,
        marginTop: 10
    },
    bloodGroupText: {
        alignSelf: 'center',
        color: 'black',
        fontWeight: 'bold'
    },
    hospitalText: {
        fontSize: 22,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 30,
        marginBottom: 5
    },
    textInputView: {
        flexDirection: 'row',
        height: 43,
        marginTop: 25,
        borderRadius: 25,
        marginHorizontal: 40,
        backgroundColor: 'white'
    },
    inputTxt: {
        color: '#808080',
        marginLeft: 10,
        fontSize: 16,
        width: '100%'
    },
    dateIcon: {
        paddingRight: 7,
    },
    slider: {
        marginHorizontal: 32,
        marginTop: 17
    },
    seekBloodBtnView: {
        borderRadius: 19,
        height: 44,
        borderColor: '#ddd',
        marginBottom: 20,
        marginHorizontal: 30,
        backgroundColor: '#fc5f64',
        elevation: 2,
        marginTop: 30,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1
        //#F69AA8  Google FB
    },
    seekBloodText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 9
    },
    sliderThumb: {
        borderWidth: 1.5,
        height: 16,
        width: 16,
        borderRadius: 16 / 2,
        borderColor: '#ff1654'
    },
    unitText: {
        fontSize: 13,
        alignSelf: 'center',
        marginVertical: 3
    }
})
export default styles;