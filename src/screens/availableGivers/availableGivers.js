import React, { Component } from 'react';
import { TouchableOpacity, View, Text, TextInput, SafeAreaView, StatusBar, ScrollView, Dimensions } from 'react-native';
import styles from './styles';
import * as Animatable from 'react-native-animatable';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
const width = Dimensions.get('window').width;
import AvailableGiversCard from '../../components/availableGiversCard/availableGiversCard';


const bloodGiversArray = [
    {
        name: 'Anshuman S',
        location: 'Mumbai, Maharashtra',
        bloodDonated: 900,
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    },
    {
        name: 'Riddhesh D',
        location: 'Thane, Maharashtra',
        bloodDonated: 800,
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    },
    {
        name: 'Prerna B',
        location: 'Mumbai, Maharashtra',
        bloodDonated: 'New',
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    },
    {
        name: 'Mohanish N',
        location: 'Mumbai, Maharashtra',
        bloodDonated: 'New',
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    },
    {
        name: 'Utkarsh P',
        location: 'Mumbai, Maharashtra',
        bloodDonated: 1500,
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    },
    {
        name: 'Rushikesh C',
        location: 'Pune, Maharashtra',
        bloodDonated: 600,
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    },
    {
        name: 'Vruti S',
        location: 'Mumbai, Maharashtra',
        bloodDonated: 'New',
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    }, {
        name: 'Anamika S',
        location: 'Mumbai, Maharashtra',
        bloodDonated: 'New',
        uri: 'https://res.cloudinary.com/dgeav5vg3/image/upload/v1582528732/Rushikesh_mdio5x.jpg'
    }
]


class AvailableGivers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isRequestCreated: true,
            showScrollToTop: false
        },
            this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        setTimeout(() => this.setState({ isRequestCreated: false }), 1500);
    }

    onBackPressed() {
        this.props.navigation.goBack()
    }

    async goToTop() {
        await this.scroll.scrollTo({ x: 0, y: 0, animated: true });
        this.setState({
            showScrollToTop: false
        })
    }

    handleScroll(event) {
        if (event.nativeEvent.contentOffset.y === 0) {
            this.setState({
                showScrollToTop: false
            })
        }
    }

    scrollToTop() {
        this.setState({
            showScrollToTop: true
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                {
                    this.state.isRequestCreated
                        ?
                        < Animatable.View animation='fadeOut' duration={2000} style={styles.safeAreaViewAnimated} >
                            <StatusBar backgroundColor='#ff1654' barStyle='dark-content' />
                            <Text allowFontScaling={false} style={styles.requestCreatedText}>Request Created!</Text>
                            <Text allowFontScaling={false} style={styles.subtitleText}>We have let our Givers and Blood Banks know that you need blood.</Text>
                        </Animatable.View >
                        :
                        <Animatable.View animation='fadeIn' style={{ flex: 1 }}>
                            <StatusBar backgroundColor='white' barStyle='dark-content' />
                            <IconAntDesign name='arrowleft' style={styles.icon} size={25} onPress={() => this.onBackPressed()} />
                            <View style={styles.mainView}>
                                <Text allowFontScaling={false} style={styles.availableGiversTitleText}>Available Givers</Text>
                                <View style={styles.potentialGiversView}>
                                    <Text style={styles.potentialGiversText}>4 POTENTIAL GIVERS FOUND</Text>
                                    <Text style={styles.contactAllText}>CONTACT ALL</Text>
                                </View>
                                <ScrollView
                                    ref={(c) => this.scroll = c}
                                    onScroll={this.handleScroll}
                                    scrollEventThrottle={16}
                                    onScrollBeginDrag={() => this.scrollToTop()}
                                >

                                    <View style={styles.bloodGiversCardView}>
                                        {
                                            bloodGiversArray.map((giversData) => {
                                                return (
                                                    <AvailableGiversCard
                                                        cardData={giversData}
                                                    />
                                                )
                                            })

                                        }
                                    </View>
                                    <View style={styles.comeBackView}>
                                        <Text style={styles.allGiversRenderedText}>That's all the Givers we have for now!</Text>
                                        <Text style={styles.comeBackLaterText}>Come back in a while and see if more people are available</Text>
                                    </View>
                                </ScrollView>

                                {
                                    this.state.showScrollToTop
                                        ?
                                        <IconMaterialCommunity
                                            name='chevron-up-circle'
                                            size={50}
                                            color='#fc5f64'
                                            onPress={() => this.goToTop()}
                                            style={{ alignSelf: 'flex-end', position: 'absolute', bottom: 60 }} />
                                        :
                                        null
                                }
                            </View>
                        </Animatable.View>
                }
            </SafeAreaView>

        )
    }
}
export default AvailableGivers;