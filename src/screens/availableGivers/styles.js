import { StyleSheet, Dimensions } from "react-native";
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    safeAreaViewAnimated: {
        flex: 1,
        backgroundColor: '#ff1654',
        justifyContent: 'center'
    },
    requestCreatedText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        fontSize: 22,
        color: '#ffffff',
    },
    subtitleText: {
        textAlign: 'center',
        color: 'white',
        marginHorizontal: 30,
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 15,
    },
    mainView: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginHorizontal: 8,
        marginBottom: 10,
        height: '93%',
        elevation: -1,
        justifyContent: 'center'
    },
    icon: {
        marginVertical: 12,
        marginLeft: 14
    },
    detailsText: {
        color: 'white',
        fontSize: 15,
        marginTop: 20,
        marginHorizontal: 30,
    },
    availableGiversTitleText: {
        fontSize: 25,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 20
    },
    titleText: {
        fontSize: 22,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 10
    },
    potentialGiversView: {
        flexDirection: 'row',
        borderRadius: 25,
        marginHorizontal: 20,
        height: 45,
        backgroundColor: 'white',
        marginVertical: 15,
        justifyContent: 'space-around'
    },
    potentialGiversText: {
        color: '#989898',
        fontSize: 11,
        fontWeight: 'bold',
        paddingVertical: 15
    },
    contactAllText: {
        color: '#ff1654',
        fontSize: 11,
        fontWeight: 'bold',
        paddingVertical: 15
    },
    bloodGiversCardView: {
        marginTop: 15,
        marginHorizontal: 20,
        bottom: 0
    },
    comeBackView: {
        alignSelf: 'center'
    },
    allGiversRenderedText: {
        fontSize: 13,
        color: '#989898',
        fontWeight: 'bold',
        alignSelf: 'center',
        marginTop: 10
    },
    comeBackLaterText: {
        fontSize: 12,
        color: '#989898',
        marginHorizontal: 60,
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 25
    },
})
export default styles;