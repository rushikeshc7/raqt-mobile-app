import { View, SafeAreaView, Text, TouchableOpacity, StatusBar } from "react-native";
import styles from './styles';
import React, { Component } from "react";

class BeGiver extends Component {
    constructor(props) {
        super(props);

    }

    onBecomeGiver() {
        this.props.navigation.navigate('seekBlood');
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <Text allowFontScaling={false} style={styles.beGiverTitle}>Become a Giver</Text>
                    <Text allowFontScaling={false} style={styles.experienceText}>Experience the joy of giving the gift of life.</Text>
                    <Text allowFontScaling={false} style={styles.detailsText}>
                        We will help you find the blood you are looking for. In meantime, why don't you consider being the superhero in someone else's life.
                        </Text>


                    <TouchableOpacity onPress={() => this.onBecomeGiver()} style={styles.beGiverView}>
                        <Text allowFontScaling={false} style={styles.beGiverText}>
                            BECOME A GIVER
                    </Text>
                    </TouchableOpacity>

                    <Text style={styles.notNowText}>NOT NOW, MAYBE LATER?</Text>
                </View>
            </SafeAreaView >
        )
    }
}

export default BeGiver;