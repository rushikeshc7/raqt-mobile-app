import React, { Component } from "react";
import { View, SafeAreaView, StatusBar, Text, TouchableOpacity, Vibration } from "react-native";
import styles from './styles';
import { connect } from "react-redux";
import CodeInput from 'react-native-confirmation-code-input';
import * as Animatable from 'react-native-animatable';

class OTPVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            OTP: '',
            animateCodeInput: false,
        }
    }

    onSubmitCode(isValid, code) {
        if (this.state.animateCodeInput) {
            Vibration.vibrate(500);
        } else {

        }
    }

    onVerify() {
        this.props.navigation.navigate('home');
    }

    onResendOTP() {

    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <Text allowFontScaling={false} style={styles.lifelineTitle}>Let's get you started with LifeLine.</Text>
                    <Text allowFontScaling={false} style={styles.verifyingPhoneText}>Verifying your phone number</Text>
                    <Text allowFontScaling={false} style={styles.smsSentText}>We sent an SMS to {this.props.phone} with a 4 digit OTP. Please enter the code below.</Text>

                    <Animatable.View animation={this.state.animateCodeInput ? 'shake' : null} duration={1000} style={styles.codeInputView}>
                        <CodeInput
                            ref={(input) => { this.codeInput = input }}
                            keyboardType="numeric"
                            codeLength={4}
                            size={43}
                            allowFontScaling={false}
                            className='border-circle'
                            // compareWithCode='1234'
                            space={20}
                            autoFocus={false}
                            selectionColor='#00BFA5'
                            codeInputStyle={styles.otpInputStyle}
                            onFulfill={(isValid, code) => this.onSubmitCode(isValid, code)}
                        />
                    </Animatable.View>
                    <TouchableOpacity onPress={() => this.onVerify()} style={styles.sendOTPView}>
                        <Text allowFontScaling={false} style={styles.sendOTPText}>
                            VERIFY
                    </Text>
                    </TouchableOpacity>
                    <Text allowFontScaling={false} style={styles.notRecievedText}>Didn't get the code?</Text>
                    <Text allowFontScaling={false} style={styles.resendOTPText} onPress={() => this.onResendOTP()}>RESEND OTP</Text>
                </View>
            </SafeAreaView >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        phone: state.login.phoneNumber
    }
}

export default connect(mapStateToProps)(OTPVerification);