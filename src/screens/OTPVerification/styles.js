import { StyleSheet, Dimensions } from "react-native";
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainView: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginHorizontal: 8,
        marginBottom: 10,
        height: '98%',
        elevation: -1,
        justifyContent: 'center'
    },
    lifelineTitle: {
        fontSize: 25,
        alignSelf: 'center',
        marginHorizontal: 30,
        fontWeight: 'bold'
    },
    verifyingPhoneText: {
        fontSize: 20,
        marginHorizontal: 30,
        fontWeight: '100',
        marginTop: 13
    },
    smsSentText: {
        color: '#aaa',
        fontSize: 16,
        marginTop: 20,
        marginHorizontal: 50,
        textAlign: 'center'
    },
    codeInputView: {
        height: 100
    },
    otpInputStyle: {
        borderWidth: 0.5,
        borderColor: '#73c2fb',
        color: 'black',
        fontSize: 18,
        fontWeight: '200'
    },
    sendOTPView: {
        borderRadius: 19,
        height: 42,
        borderColor: '#ddd',
        marginHorizontal: 30,
        backgroundColor: '#fc5f64',
        elevation: 2,
        marginBottom: 25,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1
        //#F69AA8  Google FB
    },
    sendOTPText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 7
    },
    notRecievedText: {
        color: '#aaa',
        fontSize: 16,
        marginHorizontal: 40,
        textAlign: 'center'
    },
    resendOTPText: {
        fontSize: 18,
        color: '#aaa',
        textAlign: 'center',
        fontWeight: 'bold'
    }
})
export default styles;