import React, { Component } from "react";
import { SafeAreaView, StatusBar, Text, TouchableOpacity, View, TextInput, Dimensions } from "react-native";
import styles from './styles';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import IconMaterial from "react-native-vector-icons/MaterialIcons";
const width = Dimensions.get('window').width;


class GiverInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dob: '',
            name: '',
            email: '',
            currentDate: new Date(),
            nameError: false,
            nameError2: false,
            emailError: false,
            emailError2: false,
        }
    }

    validation = () => {
        let flag = false;
        var name = /[a-zA-Z\s]/;
        var emailId = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

        //name validation
        if (this.state.name.length === 0) {
            flag = true;
            this.setState({
                errorName: true,
                errorName2: false
            })
        }
        else if (!name.test(this.state.name)) {
            flag = true;
            this.setState({
                errorName: false,
                errorName2: true
            })
        }
        else {
            flag = false;
            this.setState({
                errorName: false,
                errorName2: false
            })
        }

        //email validation
        if (this.state.email.length === 0) {
            flag = true;
            this.setState({
                errorEmail: true,
                errorEmail2: false
            })
        }
        else if (!emailId.test(this.state.email)) {
            flag = true;
            this.setState({
                errorEmail: false,
                errorEmail2: true
            })
        }
        else {
            flag = false;
            this.setState({
                errorEmail: false,
                errorEmail2: false
            })
        }
        return flag;
    }

    async onNext() {
        await this.validation();
        if (this.state.nameError) {
            alert('Enter name');
        } else if (this.state.nameError2) {
            alert('Enter valid name');
        } else if (this.state.emailError) {
            alert('Enter email');
        } else if (this.state.emailError2) {
            alert('Enter valid email');
        } else {
            this.props.navigation.navigate('giverBloodInfo');
        }
    }

    render() {
        let minDate = moment(this.state.currentDate).subtract(110, 'year').format('DD-MMM-YYYY');
        let maxDate = moment(this.state.currentDate).subtract(18, 'year').format('DD-MMM-YYYY');
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <Text allowFontScaling={false} style={styles.helloGiverTitle}>Hello Giver</Text>
                    <Text allowFontScaling={false} style={styles.thanksText}>Thanks for joining us on our mission to save lives.</Text>
                    <Text allowFontScaling={false} style={styles.detailsText}>We need a few details to help us connect you to people who need your gift of blood.</Text>
                    <Text allowFontScaling={false} style={styles.dontWorryText}>Don't worry we won't share your information unless you approve of it.</Text>

                    <View style={styles.textInputView}>
                        <TextInput
                            placeholder='Name'
                            placeholderTextColor="#CCC"
                            style={styles.inputTxt}
                            allowFontScaling={false}
                            keyboardType='default'
                            onSubmitEditing={() => {
                                this.emailInput.focus();
                            }}
                            onChangeText={(name) => this.setState({ name: name })}
                            value={this.state.name}
                            returnKeyType='next' />
                    </View>

                    <View style={styles.textInputView}>
                        <TextInput
                            ref={(emailInput) => { this.emailInput = emailInput }}
                            placeholder='Email'
                            placeholderTextColor="#CCC"
                            style={styles.inputTxt}
                            allowFontScaling={false}
                            keyboardType='email-address'
                            onSubmitEditing={() => {
                                this.dateInput.focus()
                            }}
                            onChangeText={(email) => this.setState({ email: email })}
                            value={this.state.email}
                            returnKeyType='next' />
                    </View>

                    <View style={{ marginHorizontal: 40 }}>
                        <DatePicker
                            ref={(dateInput) => { this.dateInput = dateInput }}
                            style={{ height: 43, width: '100%', marginTop: 25, backgroundColor: 'white', borderRadius: 5, }}
                            customStyles={{
                                dateText: { color: '#808080', paddingLeft: 12, alignSelf: 'flex-start', fontSize: 16, },
                                dateInput: { borderColor: 'white', borderRadius: 5 },
                                datePicker: { backgroundColor: "#cdcdcd" },
                                placeholderText: { alignSelf: 'flex-start', fontSize: 16, paddingLeft: 12 },
                            }}
                            date={this.state.dob}
                            mode="date"
                            placeholder="Birthday"
                            placeholderTextColor="#ccc"
                            allowFontScaling={false}
                            showIcon={true}
                            iconComponent={<IconMaterial name='date-range' color='#ff1654' size={30} style={styles.dateIcon} />}
                            format="DD-MMM-YYYY"
                            minDate={minDate}
                            maxDate={maxDate}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            onDateChange={(date) => { this.setState({ dob: date }) }}
                        />
                    </View>
                    <TouchableOpacity onPress={() => this.onNext()} style={styles.nextView}>
                        <Text allowFontScaling={false} style={styles.nextText}>
                            Next
                        </Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView >
        )
    }
}

export default GiverInfo;