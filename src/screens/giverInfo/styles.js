import { StyleSheet, Dimensions } from "react-native";
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainView: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginHorizontal: 8,
        marginBottom: 10,
        height: '98%',
        elevation: -1,
        justifyContent: 'center'
    },
    helloGiverTitle: {
        fontSize: 25,
        marginHorizontal: 30,
        fontWeight: 'bold'
    },
    thanksText: {
        fontSize: 20,
        marginHorizontal: 30,
        fontWeight: '100',
        marginTop: 13,
        color: '#aaa',
    },
    detailsText: {
        color: 'gray',
        fontSize: 14,
        marginTop: 20,
        marginHorizontal: 30,
    },
    dontWorryText: {
        color: 'gray',
        fontSize: 14,
        marginHorizontal: 30,
    },
    textInputView: {
        flexDirection: 'row',
        height: 43,
        marginTop: 25,
        borderRadius: 5,
        marginHorizontal: 40,
        backgroundColor: 'white'
    },
    inputTxt: {
        color: '#808080',
        marginLeft: 10,
        fontSize: 16,
        width: '100%'
    },
    dateIcon: {
      paddingRight:7,
    },
    nextView: {
        borderRadius: 19,
        height: 42,
        borderColor: '#ddd',
        marginHorizontal: 30,
        backgroundColor: '#fc5f64',
        elevation: 2,
        marginBottom: 25,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1,
        marginTop: 30
        //#F69AA8  Google FB
    },
    nextText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 7
    },
})
export default styles;