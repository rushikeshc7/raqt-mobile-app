import { View, SafeAreaView, Text, TouchableOpacity, StatusBar } from "react-native";
import styles from './styles';
import React, { Component } from "react";

class Seeker extends Component {
    constructor(props) {
        super(props);

    }

    onSeekBlood() {
        this.props.navigation.navigate('seekBlood');
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <View>
                        <Text allowFontScaling={false} style={styles.helloSeekerTitle}>Hello Seeker</Text>
                        <Text allowFontScaling={false} style={styles.missionText}>It is our mission to help you find blood for your loved ones</Text>
                        <Text allowFontScaling={false} style={styles.detailsText}>
                            We need a few more details before we start searching for blood. We will reach out to our Givers and help you find out where to get blood.
                        </Text>
                    </View>

                    <TouchableOpacity onPress={() => this.onSeekBlood()} style={styles.seekBloodView}>
                        <Text allowFontScaling={false} style={styles.seekBloodText}>
                            SEEK BLOOD
                    </Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView >
        )
    }
}

export default Seeker;