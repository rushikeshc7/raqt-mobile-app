import React, { Component } from 'react';
import { View, Text, Image, SafeAreaView, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import styles from './styles';


export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            confirmPassword: '',
            errorPassword: false,
            errorPassword2: false,
            errorConfirmPassword: false,
            errorConfirmPassword2: false,
            errorConfirmPassword3: false,
            checkValidation: true
        }
    }

    validation = () => {
        let flag = true;
        var pwd = /[a-zA-Z0-9`~!@#$%^&*()_+-=<>{}/]/

        if (this.state.checkValidation) {

            //password validation
            if (this.state.password.length === 0) {
                flag = false;
                this.setState({
                    errorPassword: true,
                    errorPassword2: false
                })
            }
            else if (this.state.password.length < 8 || !pwd.test(this.state.password)) {
                flag = false;
                this.setState({
                    errorPassword: false,
                    errorPassword2: true
                })
            }
            else {
                flag = true;
                this.setState({
                    errorPassword: false,
                    errorPassword2: false
                })
            }

            //confirm password validation
            if (this.state.confirmPassword.length === 0) {
                flag = false;
                this.setState({
                    errorConfirmPassword: true,
                    errorConfirmPassword2: false,
                    errorConfirmPassword3: false,
                })
            }
            else if (this.state.confirmPassword.length < 8 || !pwd.test(this.state.confirmPassword)) {
                flag = false;
                this.setState({
                    errorConfirmPassword: false,
                    errorConfirmPassword2: true,
                    errorConfirmPassword3: false,
                })
            }
            else if (this.state.confirmPassword !== this.state.password) {
                flag = false;
                this.setState({
                    errorConfirmPassword: false,
                    errorConfirmPassword2: false,
                    errorConfirmPassword3: true,
                })
            }
            else {
                flag = true;
                this.setState({
                    errorConfirmPassword: false,
                    errorConfirmPassword2: false,
                    errorConfirmPassword3: false,
                })
            }
        }
        return flag;
    }

    onSave() {
        if (this.validation()) {

        }
    }

    onLoginInsteadPressed() {
        this.props.navigation.navigate('login');
    }

    render() {
        return (
            <KeyboardAvoidingView>
                <SafeAreaView style={styles.safeAreaContainer}>
                    <View style={styles.emailText}>
                        <View style={styles.emailView}>
                            <IconFontAwesome name='lock' size={20} color='#777' style={styles.nameIcon} />
                            <TextInput allowFontScaling={false}
                                placeholder="Password"
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({ password: password })}
                                value={this.state.password}
                                onSubmitEditing={() => this.ConfirmPasswordTextInput}
                                returnKeyLabel="Done"
                                returnKeyType="done" />

                            <View style={styles.line} />

                            <View style={styles.errorView}>
                                {this.state.checkValidation ? <Text allowFontScaling={false}>{this.state.errorPassword == true ? <Text allowFontScaling={false} style={styles.errText}>Enter a password</Text>
                                    : <Text allowFontScaling={false}>{this.state.errorPassword2 == true ? <Text allowFontScaling={false} style={styles.errText}>Use 8 or more characters</Text> : null}</Text>}</Text> : null}
                            </View>
                        </View>

                        <View style={styles.emailView}>
                            <TextInput
                                ref={(input) => { this.ConfirmPasswordTextInput = input; }}
                                placeholder="Confirm Password"
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                allowFontScaling={false}
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({ confirmPassword: password })}
                                value={this.state.confirmPassword}
                                onSubmitEditing={() => this.onSave()}
                                returnKeyLabel="Done"
                                returnKeyType="done" />
                            <View style={styles.line} />

                            <View style={styles.errorView}>
                                {this.state.checkValidation ? <Text allowFontScaling={false}>{this.state.errorConfirmPassword == true ? <Text allowFontScaling={false} style={styles.errText}>Enter a password</Text>
                                    : <Text allowFontScaling={false}>{this.state.errorConfirmPassword2 == true ? <Text allowFontScaling={false} style={styles.errText}>Use 8 or more characters</Text>
                                        : <Text allowFontScaling={false}>{this.state.errorConfirmPassword3 == true ? <Text allowFontScaling={false} style={styles.errText}>Passwords do not match</Text>
                                            : null}</Text>}</Text>}</Text> : null}
                            </View>
                        </View>

                    </View>

                    <TouchableOpacity onPress={() => this.onSave()} style={styles.proceedBtnView}>
                        <LinearGradient start={{ x: 0.0, y: 0.5 }} end={{ x: 0.5, y: 1.0 }}
                            locations={[0, 0.6]}
                            style={{ borderRadius: 25 }}
                            colors={['#ff9a9e', '#F977CE']}>
                            <Text allowFontScaling={false}
                                style={styles.proceedText}>Save</Text>
                        </LinearGradient>
                    </TouchableOpacity>
                </SafeAreaView >
            </KeyboardAvoidingView>
        )
    }
}