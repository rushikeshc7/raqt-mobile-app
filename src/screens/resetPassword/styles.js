import { StyleSheet, Platform, Dimensions } from 'react-native';
// import Fonts from '../../assets/fonts/fonts';
import { ifIphoneX } from 'react-native-iphone-x-helper';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    safeAreaContainer: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    logoView: {
        height: 110,
        width: 110,
        alignSelf: 'center',
        marginTop: 80
    },
    emailText: {
        marginTop: 13,
        padding: 10,
    },
    emailView: {
        flexDirection: 'row',
        width: width - 90,
        height: 45,
        alignSelf: 'center',
        marginTop: 25,
    },
    emailIcon: {
        width: 20,
        height: 20,
        marginTop: 10,
        resizeMode: 'contain'
    },
    inputTxt: {
        color: '#808080',
        marginLeft: 10,
        fontSize: 16,
        marginTop: -3,
        width: width - 140,
    },
    line: {
        width: width - 90,
        height: 1,
        position: 'absolute',
        left: 0,
        bottom: 1,
        backgroundColor: '#cccccc'
    },
    proceedBtnView: {
        borderRadius: 25,
        height: 50,
        width: 180,
        marginTop: (Platform.OS == 'ios') ? 50 : 50,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    proceedText: {
        fontSize: 18,
        marginHorizontal: 50,
        marginVertical: 13,
        color: '#ffffff',
        backgroundColor: 'transparent'
    },
    errorView: {
        height: 3,
        justifyContent: 'flex-start',
        marginLeft: 50
    },
    errText: {
        color: 'red',
    },
    loginInsteadView: {
        alignSelf: 'center',
        marginTop: 15
    },
    loginInsteadText: {
        fontSize: 14,
        color: '#333333',
    },
})
export default styles;