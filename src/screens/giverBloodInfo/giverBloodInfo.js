import React, { Component } from "react";
import { SafeAreaView, TouchableOpacity, StatusBar, View, Text, FlatList, ScrollView, TouchableWithoutFeedback } from "react-native";
import styles from './styles';
import DonationCenterCard from "../../components/donationCenterCard/DonationCenterCard";

const bloodGroupArray = ['A+', 'A-', 'AB+', 'AB-', 'O+', 'O-', 'B+', 'B-'];
const bloodInfoArray = [
    {
        addressType: 'Work',
        city: 'Pune, Maharashtra',
        location: 'Pimpri'
    },
    {
        addressType: 'Home',
        city: 'Mumbai, Maharashtra',
        location: 'Mira Bhayandar'
    },
    {
        addressType: 'Office',
        city: 'Thane, Maharashtra',
        location: 'Ghodbunder Road'
    }
]



class GiverBloodInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bloodGroupSelectedIndex: 0
        }
    }

    onBloodGroupSelected(index) {
        this.setState({
            bloodGroupSelectedIndex: index
        })
    }

    onBecomeGiverPressed() {
        this.props.navigation.navigate('tutorial');
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <Text allowFontScaling={false} style={styles.titleText}>What is your blood group?</Text>
                    <View style={styles.bloodGroupView}>
                        {
                            bloodGroupArray.map((item, index) => {
                                if (index === this.state.bloodGroupSelectedIndex) {
                                    return (
                                        <TouchableOpacity>
                                            <View style={styles.selcetedCircleView}>
                                                <Text style={styles.selectedBloodGroupText}>{item}</Text>
                                            </View >
                                        </TouchableOpacity>
                                    )
                                } else {
                                    return (
                                        <TouchableOpacity onPress={() => this.onBloodGroupSelected(index)}>
                                            <View style={styles.circleView}>
                                                <Text style={styles.bloodGroupText}>{item}</Text>
                                            </View >
                                        </TouchableOpacity>
                                    )

                                }
                            })
                        }
                    </View>

                    <Text allowFontScaling={false} style={styles.whereGiveBloodText}>Where can you give blood?</Text>
                    <ScrollView contentContainerStyle={{ paddingBottom: 5 }} showsVerticalScrollIndicator={false}>
                        <View style={styles.donationCardView}>
                            {
                                bloodInfoArray.map((bloodData) => {
                                    return (
                                        <DonationCenterCard
                                            cardData={bloodData}
                                        />
                                    )
                                })

                            }
                        </View>
                    </ScrollView>
                    <TouchableOpacity onPress={() => this.onBecomeGiverPressed()}>
                        <View style={styles.beGiverBtnView}>
                            <Text allowFontScaling={false} style={styles.beGiverText}>
                                BECOME A GIVER
                    </Text>
                        </View>
                    </TouchableOpacity>
                </View>

            </SafeAreaView>
        )
    }

}
export default GiverBloodInfo;