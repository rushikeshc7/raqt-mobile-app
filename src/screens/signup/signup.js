import React, { Component } from 'react';
import { View, Text, Image, SafeAreaView, TextInput, TouchableOpacity, KeyboardAvoidingView, TouchableOpacityBase } from 'react-native';
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import styles from './styles';


export default class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            email: '',
            mobileNumber: '',
            password: '',
            showPassword: false,
            isVisible: false,
            errorName: false,
            errorName2: false,
            errorEmail: false,
            errorEmail2: false,
            errorPassword: false,
            errorPassword2: false,
            errorConfirmPassword: false,
            errorConfirmPassword2: false,
            errorContactNumber: false,
            errorContactNumber2: false,
            checkValidation: true,
            isPlatform: (Platform.OS == 'ios' ? true : false)
        }
    }

    handleShow() {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }

    onLoginInsteadPressed() {
        this.props.navigation.navigate('login');
    }

    validation = () => {
        let flag = false;

        var name = /[a-zA-Z\s]/;
        var emailId = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        var pwd = /[a-zA-Z0-9`~!@#$%^&*()_+-=<>{}/]/;
        var mobNo = /^\d{10}$/;

        if (this.state.checkValidation) {
            //name validation
            if (this.state.userName.length === 0) {
                flag = true;
                this.setState({
                    errorName: true,
                    errorName2: false
                })
            }
            else if (!name.test(this.state.userName)) {
                flag = true;
                this.setState({
                    errorName: false,
                    errorName2: true
                })
            }
            else {
                flag = false;
                this.setState({
                    errorName: false,
                    errorName2: false
                })
            }

            //email validation
            if (this.state.email.length === 0) {
                flag = true;
                this.setState({
                    errorEmail: true,
                    errorEmail2: false
                })
            }
            else if (!emailId.test(this.state.email)) {
                flag = true;
                this.setState({
                    errorEmail: false,
                    errorEmail2: true
                })
            }
            else {
                flag = false;
                this.setState({
                    errorEmail: false,
                    errorEmail2: false
                })
            }

            //password validation
            if (this.state.password.length === 0) {
                flag = true;
                this.setState({
                    errorPassword: true,
                    errorPassword2: false
                })
            }
            else if (this.state.password.length < 8 || !pwd.test(this.state.password)) {
                flag = true;
                this.setState({
                    errorPassword: false,
                    errorPassword2: true
                })
            }
            else {
                flag = false;
                this.setState({
                    errorPassword: false,
                    errorPassword2: false
                })
            }


            //contact number validation
            if (this.state.mobileNumber.length === 0) {
                flag = true;
                this.setState({
                    errorContactNumber: true,
                    errorContactNumber2: false
                })
            }
            else if (!mobNo.test(this.state.mobileNumber)) {
                flag = true;
                this.setState({
                    errorContactNumber: false,
                    errorContactNumber2: true
                })
            }
            else {
                flag = false;
                this.setState({
                    errorContactNumber: false,
                    errorContactNumber2: false
                })
            }
        }
        return flag;
    }

    onSignupPressed = async () => {
        if (!this.validation()) {
            var registeredData = await registerUser(this.state.userName, this.state.email, this.state.mobileNumber, this.state.password);
            if (registeredData !== '' && registeredData !== undefined) {
                Snackbar.show({
                    text: 'Registration Failed',
                    duration: Snackbar.LENGTH_SHORT,
                    action: {
                        text: 'Failed',
                        textColor: 'red',
                    }
                })
                this.setState({
                    password: '',
                })

            } else {
                Snackbar.show({
                    text: 'Registration Successful',
                    duration: Snackbar.LENGTH_SHORT,
                    action: {
                        text: 'Success',
                        textColor: 'green',
                    }
                })
                this.props.navigation.navigate('login');
                this.setState({
                    userName: '',
                    email: '',
                    password: '',
                    mobileNumber: '',
                })
            }
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={styles.keyboardContainer} >
                <SafeAreaView style={styles.safeAreaContainer}>
                    <Image source={require('../../assets/heartLogo.png')} style={styles.logoView} />
                    <View style={styles.emailText}>

                        <View style={styles.emailView}>
                            <IconFontAwesome name='user' size={20} color='#777' style={styles.nameIcon} />
                            <TextInput allowFontScaling={false}
                                placeholder="Full Name"
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                keyboardType="default"
                                onSubmitEditing={() => {
                                    this.emailTextInput.focus();
                                }}
                                onChangeText={(name) => this.setState({ userName: name })}
                                value={this.state.userName}
                                returnKeyType="next" />
                            <View style={styles.line} />
                        </View>

                        <View style={styles.errorView}>
                            {this.state.checkValidation ? <Text allowFontScaling={false}>{this.state.errorName == true ? <Text allowFontScaling={false} style={styles.errText}>Enter name</Text>
                                : <Text allowFontScaling={false}>{this.state.errorName2 == true ? <Text allowFontScaling={false} style={styles.errText}>Use alphabets only</Text> : null}</Text>}</Text> : null}
                        </View>

                        <View style={styles.emailView}>
                            <IconMaterialIcons name='email' size={20} color='#777' style={styles.emailIcon} />
                            <TextInput allowFontScaling={false}
                                placeholder="Email"
                                ref={(input) => { this.emailTextInput = input; }}
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                keyboardType="email-address"
                                onSubmitEditing={() => {
                                    this.phoneTextInput.focus();
                                }}
                                onChangeText={(email) => this.setState({ email: email })}
                                value={this.state.email}
                                returnKeyType="next" />
                            <View style={styles.line} />
                        </View>

                        <View style={styles.emailView}>
                            <IconMaterialIcons name='smartphone' size={20} color='#777' style={styles.emailIcon} />
                            <TextInput allowFontScaling={false}
                                ref={(input) => { this.phoneTextInput = input; }}
                                placeholder="Phone"
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                keyboardType="number-pad"
                                maxLength={10}
                                onSubmitEditing={() => {
                                    this.addressTextInput.focus();
                                }}
                                onChangeText={(phone) => this.setState({ mobileNumber: phone })}
                                value={this.state.mobileNumber}
                                returnKeyType="next" />
                            <View style={styles.line} />
                        </View>

                        <View style={styles.emailView}>
                            <IconFontAwesome name='lock' size={20} color='#777' style={styles.nameIcon} />
                            <TextInput allowFontScaling={false}
                                ref={(input) => { this.passwordTextInput = input; }}
                                placeholder="Password"
                                placeholderTextColor="#CCC"
                                style={styles.inputTxt}
                                secureTextEntry={!this.state.showPassword}
                                onChangeText={(password) => this.setState({ password: password })}
                                value={this.state.password}
                                onSubmitEditing={() => this.onSignupPressed()}
                                returnKeyLabel="Done"
                                returnKeyType="done" />

                            <TouchableOpacity activeOpacity={0.8} style={styles.visibilityBtn} onPress={() => this.handleShow()}>
                                <IconFontAwesome name={!this.state.showPassword ? 'eye' : 'eye-slash'} style={styles.btnImage} size={20} />
                            </TouchableOpacity>
                            <View style={styles.line} />
                        </View>

                    </View>

                    {/* <TouchableOpacity onPress={() => this.onSignupPressed()} style={styles.signupBtnView}>
                        <LinearGradient start={{ x: 0.0, y: 0.5 }} end={{ x: 0.5, y: 1.0 }}
                            locations={[0, 0.6]}
                            style={{ borderRadius: 25 }}
                            colors={['#ff9a9e', '#F977CE']}>
                            <Text allowFontScaling={false} style={styles.signupText}>Sign up</Text>
                        </LinearGradient>
                    </TouchableOpacity> */}

                    <View style={styles.loginInsteadView}>
                        <TouchableOpacity onPress={() => this.onLoginInsteadPressed()}>
                            <Text allowFontScaling={false} style={styles.loginInsteadText}>Login Instead?</Text>
                        </TouchableOpacity>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>


        )
    }
}

