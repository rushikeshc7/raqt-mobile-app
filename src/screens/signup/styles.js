import { StyleSheet, Platform, Dimensions } from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    safeAreaContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    keyboardContainer: {
        flex: 1,
        justifyContent: 'center'
    },
    logoView: {
        height: 110,
        width: 110,
        alignSelf: 'center',
    },
    emailView: {
        flexDirection: 'row',
        width: width - 90,
        height: 45,
        alignSelf: 'center',
        marginTop: 25,
    },
    emailIcon: {
        width: 20,
        height: 20,
        marginTop: 10,
        resizeMode: 'contain',
        marginLeft: 3
    },
    nameIcon: {
        width: 20,
        height: 20,
        marginTop: 10,
        resizeMode: 'contain',
        marginLeft: 5
    },
    inputTxt: {
        color: '#808080',
        marginLeft: 10,
        fontSize: 16,
        marginTop: -3,
        width: width - 140,
    },
    line: {
        width: width - 90,
        height: 1,
        position: 'absolute',
        left: 0,
        bottom: 1,
        backgroundColor: '#cccccc'
    },
    signupBtnView: {
        borderRadius: 25,
        height: 50,
        width: 180,
        marginTop: (Platform.OS == 'ios') ? 50 : 30,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    signupText: {
        fontSize: 18,
        marginHorizontal: 65,
        marginVertical: 13,
        color: '#ffffff',
        backgroundColor: 'transparent'
    },
    btnImage: {
        marginTop: 8,
        height: 30,
        width: 30,
        marginRight: 20,
        color: '#aaa'
    },
    visibilityBtn: {
        position: 'absolute',
        right: 13,
        width: 25,
        height: 50,
        padding: 5,
    },
    phoneIcon: {
        paddingTop: 12,
        borderBottomWidth: 0.73,
        borderColor: "#999999",
    },
    phoneIconText: {
        fontSize: 16,
        color: "#bbb",
    },
    signupBtnView: {
        borderRadius: 25,
        height: 50,
        width: 180,
        marginTop: (Platform.OS == 'ios') ? 40 : 40,
        flexDirection: 'row',
        alignSelf: 'center'
    },
    signupText: {
        fontSize: 18,
        marginHorizontal: 55,
        marginVertical: 13,
        color: '#ffffff',
        backgroundColor: 'transparent'
    },
    loginInsteadView: {
        alignSelf: 'center',
        marginTop: 15
    },
    loginInsteadText: {
        fontSize: 14,
        color: '#333333',
    },
    errorView: {
        height: 5,
        marginLeft: 50
    },
    errText: {
        color: 'red'
    },

})

export default styles;