import { StyleSheet, Dimensions } from "react-native";
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainView: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginHorizontal: 8,
        marginBottom: 10,
        height: '98%',
        elevation: -1,
        justifyContent: 'center'
    },
    lifelineTitle: {
        fontSize: 25,
        alignSelf: 'center',
        marginHorizontal: 30,
        fontWeight: 'bold'
    },
    howToContactText: {
        fontSize: 20,
        marginHorizontal: 30,
        fontWeight: '100',
        marginTop: 13
    },
    phoneTextInputView: {
        flexDirection: 'row',
        height: 40,
        marginTop: 70,
        marginHorizontal: 30
    },
    countryCodeView: {
        borderColor: '#73c2fb',
        borderTopWidth: 0.7,
        borderBottomWidth: 0.7,
        borderLeftWidth: 0.7,
        borderRightWidth: 0.35,
        height: 40,
        width: '15%'
    },
    countryCodeText: {
        fontSize: 18,
        paddingVertical: 6,
        alignSelf: 'center'
    },
    phoneTextInput: {
        fontSize: 18,
        borderTopWidth: 0.7,
        borderBottomWidth: 0.7,
        borderLeftWidth: 0.35,
        borderRightWidth: 0.7,
        borderColor: '#73c2fb',
        width: '85%',
        paddingVertical: 6,
        paddingLeft: 15
    },
    sendOTPView: {
        borderRadius: 19,
        height: 42,
        borderColor: '#ddd',
        marginTop: 30,
        marginHorizontal: 30,
        backgroundColor: '#fc5f64',
        elevation: 2,
        marginBottom: 25,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1
        //#F69AA8  Google FB
    },
    sendOTPText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 7
    },
    otpVerifyText: {
        color: '#ccc',
        fontSize: 17,
        marginHorizontal: 40,
        textAlign: 'center'
    }
})

export default styles;