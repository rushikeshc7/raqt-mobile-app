import React, { Component } from "react";
import { SafeAreaView, Text, View, StatusBar, TextInput, TouchableOpacity } from "react-native";
import styles from "./styles";
import { connect } from "react-redux";

class ContactNumber extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: this.props.phone
        }
    }

    onSendOTP() {
        this.props.navigation.navigate('otpVerification');
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <Text allowFontScaling={false} style={styles.lifelineTitle}>Let's get you started with LifeLine.</Text>
                    <Text allowFontScaling={false} style={styles.howToContactText}>How can we contact you?</Text>
                    <View style={styles.phoneTextInputView}>
                        <View style={styles.countryCodeView}>
                            <Text style={styles.countryCodeText}>+91</Text>
                        </View>
                        <TextInput
                            allowFontScaling={false}
                            placeholder='Phone Number'
                            placeholderTextColor='#ccc'
                            style={styles.phoneTextInput}
                            defaultValue={this.state.phone}
                            onChangeText={(phone) => this.setState({ phone: phone })}
                            onSubmitEditing={() => this.onSendOTP()}
                            maxLength={10}
                            returnKeyType='go'
                            value={this.state.phone}
                        />
                    </View>

                    <TouchableOpacity onPress={() => this.onSendOTP()} style={styles.sendOTPView}>
                        <Text allowFontScaling={false} style={styles.sendOTPText}>
                            SEND OTP
                            </Text>
                    </TouchableOpacity>

                    <Text style={styles.otpVerifyText}>We will send you an OTP to your phone for verification</Text>

                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        phone: state.login.phoneNumber,
    }
}

export default connect(mapStateToProps)(ContactNumber);