import { StyleSheet, Platform, Dimensions } from 'react-native';
// import { ifIphoneX } from 'react-native-iphone-x-helper';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    SafeAreaView: {
        flex: 1,
        justifyContent: 'center',
        // backgroundColor: '#e62e56'
        backgroundColor: '#ff1654'
    },
    logoView: {
        alignSelf: 'center',
    },
    titleText: {
        fontSize: 35,
        alignSelf: 'center',
        marginTop: -19,
        marginBottom: 10,
        color: '#ffffff',
    },
    phoneView: {
        flexDirection: 'row',
        height: 43,
        marginTop: 25,
        borderRadius: 5,
        marginHorizontal: 40,
        backgroundColor: 'white'
    },
    inputTxt: {
        color: '#808080',
        marginLeft: 10,
        fontSize: 16,
        width: '100%'
    },
    loginBtnView: {
        borderRadius: 19,
        height: 44,
        borderColor: '#ddd',
        marginTop: 17,
        marginHorizontal: 40,
        backgroundColor: '#fc5f64',
        elevation: 2,
        marginBottom: 25,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1
        //#F69AA8  Google FB
    },
    loginText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 9
    },
    socialLoginView: {
        flexDirection: 'row',
        marginTop: 7,
        marginHorizontal: 90,
        height: 40,
        borderRadius: 19,
        backgroundColor: '#F69AA8'
    },
    fbLogo: {
        height: 25,
        width: 25,
        marginLeft: 10,
        marginVertical: 8,
        marginRight: 14
    },
    googleLogo: {
        height: 22,
        width: 22,
        marginLeft: 11,
        marginVertical: 9,
        marginRight: 16
    },
    socialLoginText: {
        fontSize: 13,
        alignSelf: 'center',
        fontWeight: 'bold',
        paddingVertical: 5,
        color: 'white'
    },
    loginWithEmailText: {
        fontSize: 14,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        marginVertical: 17
    },
    line: {
        width: width - 160,
        height: 1,
        alignSelf: 'center',
        marginTop: 3,
        backgroundColor: 'white'
    },
    newUserView: {
        alignSelf: 'center',
        marginTop: 5,
        flexDirection: 'row'
    },
    newHereText: {
        fontSize: 14,
        color: '#eee',
        fontWeight: '100'
    },
    signupText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'white',
        marginLeft: 5,
    },
    searchForBloodView: {
        alignSelf: 'center',
        marginTop: 10,
        flexDirection: 'row'
    },
    skipText: {
        fontSize: 14,
        color: '#eee',
    },
    searchForBloodText: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'white',
        marginLeft: 5,
    },
})

export default styles;