import React, { Component } from 'react';
import { View, SafeAreaView, Text, Image, TouchableOpacity, TextInput, StatusBar } from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { SET_PHONE_NUMBER } from '../../redux/actionTypes/actionTypes';
import I18n from '../../i18n/i18n';
import {
    AccessToken,
    LoginManager,
} from 'react-native-fbsdk';
import {
    GoogleSignin,
    statusCodes,
} from 'react-native-google-signin';


const mapDispatchToProps = (dispatch) => ({
    onSetPhone: phone =>
        dispatch({ type: SET_PHONE_NUMBER, phoneNumber: phone })

})

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: '',
            phoneError: '',
            phoneError2: '',
            userInfo: null,
            gettingLoginStatus: true,
            userName: '',
            token: '',
            profilePic: '',
        }

    }

    componentDidMount() {
        GoogleSignin.configure({
            scopes: ['https://www.googleapis.com/auth/drive.readonly'],
            webClientId: '90595589400-mokkqqdgmspt15vm0doskgdgca1bih8v.apps.googleusercontent.com',
        });
        this._isSignedIn();
    }

    getResponseInfo = (error, result) => {
        if (error) {
            //Alert for the Error
            Alert.alert('Error fetching data: ' + error.toString());
        } else {
            //response alert
            alert(JSON.stringify(result));
            this.setState({ user_name: 'Welcome' + ' ' + result.name });
            this.setState({ token: 'User Token: ' + ' ' + result.id });
            this.setState({ profile_pic: result.picture.data.url });
        }
    };

    onLogout = () => {
        //Clear the state after logout
        this.setState({ user_name: null, token: null, profile_pic: null });
    };

    _isSignedIn = async () => {
        const isSignedIn = await GoogleSignin.isSignedIn();
        if (isSignedIn) {
            alert('User is already signed in');
            this._getCurrentUserInfo();
        } else {
            console.log('Please Login');
        }
        this.setState({ gettingLoginStatus: false });
    };

    _getCurrentUserInfo = async () => {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            console.log('User Info --> ', userInfo);
            this.setState({ userInfo: userInfo });
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                alert('User has not signed in yet');
                console.log('User has not signed in yet');
            } else {
                alert("Something went wrong. Unable to get user's info");
                console.log("Something went wrong. Unable to get user's info");
            }
        }
    };

    onGoogleSignin = async () => {
        try {
            await GoogleSignin.hasPlayServices({
                showPlayServicesUpdateDialog: true,
            });
            const userInfo = await GoogleSignin.signIn();
            console.log('User Info --> ', userInfo);
            this.setState({ userInfo: userInfo });
        } catch (error) {
            console.log('Message', error.message);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                console.log('User Cancelled the Login Flow');
            } else if (error.code === statusCodes.IN_PROGRESS) {
                console.log('Signing In');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                console.log('Play Services Not Available or Outdated');
            } else {
                console.log('Some Other Error Happened');
            }
        }
    };

    _signOut = async () => {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            this.setState({ userInfo: null });
        } catch (error) {
            console.error(error);
        }
    };

    onFacebookSignin() {
        LoginManager.setLoginBehavior('native_with_fallback');
        LoginManager.logInWithPermissions(["public_profile"]).then(
            function (result) {
                if (result.isCancelled) {
                    console.log("Login cancelled");
                } else {
                    console.log(
                        "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                    console.log('FBResult:', result);
                    AccessToken.getCurrentAccessToken().then((accessToken) => {
                        console.log('AccessToken:', accessToken)
                    })
                }
            },
            function (error) {
                console.log("Login fail with error: " + error);
            }
        )
    }


    onSignupPressed() {
        this.props.navigation.navigate('signup');
    }

    validation = () => {
        let flag = false;
        var mobileNumber = /^\d{10}$/;

        if (this.state.phone.length === 0) {
            flag = true;
            this.setState({
                phoneError: true,
                phoneError2: false
            })
        }
        else if (!mobileNumber.test(this.state.phone)) {
            flag = true;
            this.setState({
                phoneError: false,
                phoneError2: true
            })
        }
        else {
            flag = false;
            this.setState({
                phoneError: false,
                phoneError2: false
            })
        }
        return flag;
    }

    onLogin() {
        this.props.navigation.navigate('contactNumber')
    }

    onLoginPressed() {
        if (!this.validation()) {
            this.props.onSetPhone(this.state.phone);
            this.props.navigation.navigate('contactNumber');
        }
        else {
            alert('Enter valid phone Number');
        }
    }


    render() {
        // if (this.state.userInfo !== null) {
        //     alert(`Name: ${this.state.userInfo.user.name}\n\n Email: ${this.state.userInfo.user.email}`)
        // }
        return (
            <SafeAreaView style={styles.SafeAreaView} >
                <StatusBar backgroundColor='#ff1654' barStyle='light-content' />
                {/* <Image source={require('../../assets/heartLogo.png')} style={styles.logoView} /> */}
                <IconFontAwesome name='heartbeat' color='white' size={130} style={styles.logoView} />
                <View style={{ marginTop: 30 }}>
                    <Text allowFontScaling={false} style={styles.titleText}>
                        LifeLine
                    </Text>
                </View>

                <View style={styles.phoneView}>
                    <TextInput
                        placeholder={I18n.t('phoneNumber')}
                        placeholderTextColor="#CCC"
                        style={styles.inputTxt}
                        allowFontScaling={false}
                        maxLength={10}
                        keyboardType='phone-pad'
                        onSubmitEditing={() => {
                            this.onLoginPressed();
                        }}
                        onChangeText={(phone) => this.setState({ phone: phone })}
                        value={this.state.phone}
                        returnKeyType='done' />
                </View>

                <TouchableOpacity onPress={() => this.onLoginPressed()} style={styles.loginBtnView}>
                    <Text allowFontScaling={false} style={styles.loginText}>
                        {I18n.t('login')}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.onFacebookSignin()} style={styles.socialLoginView}>
                    <Image source={require('../../assets/fbLogo.png')} style={styles.fbLogo} />
                    <Text allowFontScaling={false} style={styles.socialLoginText}>
                        {I18n.t('loginWithFB')}
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.onGoogleSignin()} style={styles.socialLoginView}>
                    <Image source={require('../../assets/googleLogo.png')} style={styles.googleLogo} />
                    <Text allowFontScaling={false} style={styles.socialLoginText}>
                        {I18n.t('loginWithGoogle')}
                    </Text>
                </TouchableOpacity>

                <Text allowFontScaling={false} onPress={() => this.onLoginWithEmail()} style={styles.loginWithEmailText}>
                    {I18n.t('loginWithEmail')}
                </Text>
                <View style={styles.line} />

                <View style={styles.newUserView}>
                    <Text allowFontScaling={false} style={styles.newHereText}>{I18n.t('newHere')}</Text>
                    <TouchableOpacity onPress={() => this.onSignupPressed()}>
                        <Text allowFontScaling={false} style={styles.signupText} >{I18n.t('signUp')}</Text>
                    </TouchableOpacity>
                </View>

                <View style={styles.searchForBloodView}>
                    <Text allowFontScaling={false} style={styles.skipText}>{I18n.t('skipSignUp')}</Text>
                    <TouchableOpacity onPress={() => this.onSignupPressed()}>
                        <Text allowFontScaling={false} style={styles.searchForBloodText} >{I18n.t('searchForBlood')}</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}



export default connect(undefined, mapDispatchToProps)(Login);

// WebClientId
// 90595589400-mokkqqdgmspt15vm0doskgdgca1bih8v.apps.googleusercontent.com

//WebClientSecret
//Cn6qfq5GBeC33wcFvYl2tllG