import React, { Component } from "react";
import { SafeAreaView, Text, View } from "react-native";
import styles from './styles';
import { connect } from "react-redux";
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import { Card } from 'react-native-shadow-cards';

class ContactGivers extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        console.log('dateOfRequest:', this.props.dateOfRequest);
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <Card style={styles.bloodRequestDetailView}>
                    <Text style={styles.requestedDate}>Started Request on {this.props.dateOfRequest}</Text>
                    <Text style={styles.nameText}>Anshuman S</Text>
                    <Text style={styles.ageText}>26 years old</Text>

                    <View style={styles.bloodGroupView}>
                        <View style={styles.circleView}>
                            <Text style={styles.bloodGroupText}>{this.props.bloodGroup}</Text>
                        </View>
                        <View style={{ marginBottom: 25, }}>
                            <View style={styles.bloodIconNameView}>
                                <IconFontAwesome name='hospital-o' size={25} color='#ff1654' />
                                <Text style={styles.bloodText}>{this.props.admittedHospital}</Text>
                            </View>
                            <View style={styles.bloodIconNameView}>
                                <IconIonicons name='md-time' size={25} color='#ff1654' />
                                <Text style={styles.bloodText}>{this.props.requiredDateOfBlood}</Text>
                            </View>
                            <View style={styles.bloodIconNameView}>
                                <IconIonicons name='ios-water' size={25} color='#ff1654' style={styles.bloodIcon} />
                                <Text style={styles.bloodText}>300 Blood Points</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.noteToGiversView}>
                        <Text style={styles.noteToGiversText}>NOTE TO GIVERS</Text>
                        <Text style={styles.noteText}>Hello. I urgently need blood for my brother. Please help.</Text>
                    </View>
                </Card>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        dateOfRequest: state.seekingBlood.dateOfRequest,
        bloodGroup: state.seekBlood.selectedBloodGroup,
        admittedHospital: state.seekBlood.admittedHospital,
        requiredDateOfBlood: state.seekBlood.requiredDateOfBlood,
        requiredUnitsOfBlood: state.seekBlood.requiredUnitsOfBlood
    }
}

export default connect(mapStateToProps)(ContactGivers);