import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    bloodRequestDetailView: {
        width: '100%',
        paddingBottom: 20,
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        elevation: 7
    },
    requestedDate: {
        fontSize: 12,
        fontStyle: 'italic',
        color: '#989898',
        alignSelf: 'center',
    },
    nameText: {
        fontSize: 22,
        fontWeight: 'bold',
        alignSelf: 'center'
    },
    ageText: {
        fontSize: 12,
        fontWeight: 'bold',
        alignSelf: 'center',
        color: '#888888',
        fontStyle: 'italic'
    },
    bloodGroupView: {
        flexDirection: 'row',
        marginTop: 7,
        marginHorizontal: 50,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    circleView: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        backgroundColor: '#ff1654',
        paddingVertical: 15,
        marginTop: -12
    },
    bloodGroupText: {
        alignSelf: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    bloodIconNameView: {
        marginTop: 10,
        flexDirection: 'row'
    },
    bloodIcon: {
        marginLeft: 2
    },
    bloodText: {
        fontSize: 14,
        marginLeft: 18,
        marginTop: 3,
        fontWeight: '600',
    },
    noteToGiversView: {
        marginHorizontal: 30,
        borderRadius: 10,
        borderWidth: 0.7,
        borderColor: '#00BFA5'
    },
    noteToGiversText: {
        color: '#ff1654',
        marginLeft: 12,
        fontWeight: 'bold',
        marginTop: 7
    },
    noteText: {
        marginLeft: 12,
        marginTop: 5,
        marginBottom: 13
    }
})

export default styles;