import { StyleSheet, Dimensions } from "react-native";
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    arrowLeft: {
        marginLeft: 15,
        marginTop: 15
    },
    mainView: {
        flex: 1,
        backgroundColor: '#f8f8f8',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginHorizontal: 8,
        marginBottom: 10,
        marginTop: 16,
        height: '98%',
        elevation: -1,
        justifyContent: 'center'
    },
    welcomeTitle: {
        fontSize: 25,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: -18
    },
    subtitleText: {
        fontSize: 20,
        marginHorizontal: 32,
        fontWeight: '100',
        marginTop: 13
    },
    giverSeekerView: {
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 90,
    },
    giverCard: {
        height: 150,
        width: 120,
        borderRadius: 18,
        marginRight: 15,
        backgroundColor: '#ff1654',
    },
    seekerCard: {
        height: 150,
        width: 120,
        borderRadius: 18,
        marginLeft: 15,
        backgroundColor: '#ff1654',
    },
    giverIconView: {
        flex: 1,
        justifyContent: 'space-evenly',
        height: '70%'
    },
    waterIcon: {
        alignSelf: 'flex-end',
        marginRight: 18,
        marginTop: 70,
        zIndex: 1,
    },
    handIcon: {
        alignSelf: 'center',
        zIndex: 0,
        marginBottom: 70
    },
    giverTextView: {
        height: '30%'
    },
    giverText: {
        alignSelf: 'center',
        fontSize: 20,
        fontWeight: 'bold',
        color: 'white',
    },
    waterOutlineIcon: {
        alignSelf: 'flex-end',
        marginRight: 18,
        marginTop: 60,
        zIndex: 1,
    },
    searchIcon: {
        alignSelf: 'center',
        zIndex: 0,
        marginBottom: 50
    },
    seekerText: {
        color: 'white'
    }
})

export default styles;