import React, { Component } from "react";
import { SafeAreaView, Text, View, StatusBar, TextInput, TouchableOpacity, BackHandler } from "react-native";
import styles from "./styles";
import { connect } from "react-redux";
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import IconFeather from 'react-native-vector-icons/Feather'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phone: this.props.phone
        }
    }

    goBack() {
        BackHandler.exitApp();
    }

    onGiverPressed() {
        this.props.navigation.navigate('giverInfo');
    }

    onSeekerPressed() {
        this.props.navigation.navigate('seekBlood');
    }

    onSendOTP() {
        this.props.navigation.navigate('otpVerification');
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <TouchableOpacity onPress={() => this.goBack()}>
                    <IconFeather name='arrow-left' style={styles.arrowLeft} size={24} />
                </TouchableOpacity>
                <StatusBar backgroundColor='white' barStyle='dark-content' />
                <View style={styles.mainView}>
                    <Text allowFontScaling={false} style={styles.welcomeTitle}>Welcome to LifeLine.</Text>
                    <Text allowFontScaling={false} style={styles.subtitleText}>Are you seeking blood or do you want to give blood?</Text>
                    <View style={styles.giverSeekerView}>
                        <TouchableOpacity style={styles.giverCard} onPress={() => this.onGiverPressed()}>
                            <View style={styles.giverIconView}>
                                <IconMaterialCommunity name='water' size={40} color='white' style={styles.waterIcon} />
                                <IconFontAwesome5 name='hand-holding' size={70} color='white' style={styles.handIcon} />
                            </View>
                            <View style={styles.giverTextView}>
                                <Text style={styles.giverText}>Giver</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.seekerCard} onPress={() => this.onSeekerPressed()}>
                            <View style={styles.giverIconView}>
                                <IconMaterialCommunity name='water' size={50} color='white' style={styles.waterOutlineIcon} />
                                <IconFeather name='search' size={60} color='white' style={styles.searchIcon} />
                            </View>
                            <View style={styles.giverTextView}>
                                <Text style={styles.giverText}>Seeker</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            </SafeAreaView>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        phone: state.login.phoneNumber,
    }
}

export default connect(mapStateToProps)(Home);