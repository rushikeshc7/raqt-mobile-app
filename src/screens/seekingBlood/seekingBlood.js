import React, { Component } from "react";
import styles from './styles'
import { View, SafeAreaView, StatusBar, Text, ScrollView, TextInput, TouchableOpacity } from "react-native";
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import { SET_BLOOD_REQUEST_DATE } from "../../redux/actionTypes/actionTypes";
import { connect } from "react-redux";
import moment from "moment";

const mapDispatchToProps = (dispatch) => ({
    onCreateRequest: value =>
        dispatch({ type: SET_BLOOD_REQUEST_DATE, bloodRequestedDate: value })
})


class SeekingBlood extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            age: '',
            noteToGiver: '',
            currentDate: Date.now()
        }
    }

    componentDidMount() {
        console.log('bloodGrp:', this.props.bloodGroup);
        console.log('units:', this.props.requiredUnitsOfBlood);
    }

    onCreateRequest() {
        this.props.navigation.navigate('availableGivers');
        this.props.onCreateRequest(moment(this.state.currentDate).format('MMM D'));
    }

    render() {
        return (
            <SafeAreaView style={styles.safeAreaView}>
                <StatusBar backgroundColor='#ff1654' barStyle='dark-content' />
                <View style={styles.seekingBloodView}>
                    <Text allowFontScaling={false} style={styles.seekingBloodTitleText}>Seeking Blood</Text>
                    <View style={{ marginBottom: 25 }}>
                        <View style={styles.bloodIconNameView}>
                            <IconIonicons name='ios-water' size={28} color='white' style={styles.bloodIcon} />
                            <Text style={styles.bloodText}>{this.props.bloodGroup} Blood</Text>
                        </View>
                        <View style={styles.bloodIconNameView}>
                            <IconFontAwesome name='hospital-o' size={28} color='white' />
                            <Text style={styles.bloodText}>{this.props.admittedHospital}</Text>
                        </View>
                        <View style={styles.bloodIconNameView}>
                            <IconIonicons name='md-time' size={28} color='white' />
                            <Text style={styles.bloodText}>{this.props.requiredDateOfBlood}</Text>
                        </View>
                    </View>
                </View>

                <ScrollView contentContainerStyle={styles.mainView}>
                    <View style={styles.giverFoundView}>
                        <View style={styles.giverValueTextView}>
                            <Text style={styles.giverValueText}>15</Text>
                            <Text style={styles.giverText}>Potential Givers Found</Text>
                        </View>
                        <View style={styles.giverValueTextView}>
                            <Text style={styles.giverValueText}>4</Text>
                            <Text style={styles.giverText}>Blood Banks Found</Text>
                        </View>
                    </View>

                    <Text allowFontScaling={false} style={styles.patientDetailsTitle}>Enter the patient details</Text>
                    <Text allowFontScaling={false} style={styles.subtitleText}>
                        Let us know a little bit about the person you are seeking blood for.
                        </Text>

                    <View style={styles.textInputView}>
                        <TextInput
                            placeholder='Name'
                            placeholderTextColor="#CCC"
                            style={styles.inputTxt}
                            allowFontScaling={false}
                            keyboardType='default'
                            onSubmitEditing={() => {
                                this.age.focus()
                            }}
                            onChangeText={(name) => this.setState({ name: name })}
                            value={this.state.name}
                            returnKeyType='next' />
                    </View>

                    <View style={styles.textInputView}>
                        <TextInput
                            ref={(input) => this.age = input}
                            placeholder='Enter Hospital'
                            placeholderTextColor="#CCC"
                            style={styles.inputTxt}
                            allowFontScaling={false}
                            maxLength={3}
                            keyboardType='number-pad'
                            onSubmitEditing={() => {
                                this.note.focus()
                            }}
                            onChangeText={(age) => this.setState({ age: age })}
                            value={this.state.age}
                            returnKeyType='next' />
                    </View>

                    <View style={styles.noteInputView}>
                        <TextInput
                            ref={(input) => this.note = input}
                            placeholder='Note to the Givers or Banks'
                            placeholderTextColor="#CCC"
                            multiline
                            style={styles.noteInputTxt}
                            allowFontScaling={false}
                            keyboardType='default'
                            onSubmitEditing={() => {
                                this.onCreateRequest()
                            }}
                            onChangeText={(noteToGiver) => this.setState({ noteToGiver: noteToGiver })}
                            value={this.state.noteToGiver}
                            returnKeyType='go' />
                    </View>

                    <TouchableOpacity onPress={() => this.onCreateRequest()}>
                        <View style={styles.createRequestBtnView}>
                            <Text allowFontScaling={false} style={styles.createRequestText}>
                                CREATE REQUEST
                                </Text>
                        </View>
                    </TouchableOpacity>

                </ScrollView>
            </SafeAreaView>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        bloodGroup: state.seekBlood.selectedBloodGroup,
        admittedHospital: state.seekBlood.admittedHospital,
        requiredDateOfBlood: state.seekBlood.requiredDateOfBlood,
        requiredUnitsOfBlood: state.seekBlood.requiredUnitsOfBlood
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeekingBlood);