import { StyleSheet, Dimensions } from "react-native";
const width = Dimensions.get('window').width;

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        backgroundColor: 'white'
    },
    mainView: {
        backgroundColor: '#f8f8f8',
    },
    seekingBloodView: {
        height: 'auto',
        backgroundColor: '#ff1654',
        elevation: 5
    },
    seekingBloodTitleText: {
        fontSize: 25,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 10,
        color: 'white'
    },
    bloodIconNameView: {
        marginHorizontal: 40,
        marginTop: 10,
        flexDirection: 'row'
    },
    bloodIcon: {
        marginLeft: 2
    },
    bloodText: {
        color: 'white',
        fontSize: 18,
        marginLeft: 18,
        fontWeight: '600',
    },
    giverFoundView: {
        height: 130,
        marginHorizontal: 35,
        marginTop: 50,
        backgroundColor: 'white',
        flexDirection: 'row',
    },
    giverValueTextView: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: 13
    },
    giverValueText: {
        alignSelf: 'center',
        color: '#ff1654',
        fontSize: 30,
        fontWeight: 'bold'
    },
    giverText: {
        color: '#989898',
        marginTop: 5,
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    patientDetailsTitle: {
        fontSize: 22,
        marginHorizontal: 32,
        fontWeight: 'bold',
        marginTop: 30
    },
    subtitleText: {
        fontSize: 18,
        marginTop: 18,
        marginHorizontal: 30,
    },
    textInputView: {
        flexDirection: 'row',
        height: 43,
        marginTop: 25,
        borderRadius: 5,
        marginHorizontal: 40,
        backgroundColor: 'white',
        borderWidth: 0.7,
        borderColor: 'black'
    },
    inputTxt: {
        color: '#808080',
        paddingHorizontal: 10,
        fontSize: 16,
        width: '100%'
    },
    noteInputView: {
        height: 129,
        marginTop: 25,
        borderRadius: 5,
        marginHorizontal: 40,
        backgroundColor: 'white',
        borderWidth: 0.7,
        borderColor: 'black'
    },
    noteInputTxt: {
        color: '#808080',
        paddingHorizontal: 10,
        fontSize: 16,
        width: '100%',
        height: '100%',
        textAlignVertical:'top'
    },
    createRequestBtnView: {
        borderRadius: 19,
        height: 44,
        borderColor: '#ddd',
        marginBottom: 20,
        marginHorizontal: 30,
        backgroundColor: '#fc5f64',
        elevation: 2,
        marginTop: 45,
        shadowColor: '#000',
        shadowOffset: { width: width - 80, height: 40 },
        shadowOpacity: 1
        //#F69AA8  Google FB
    },
    createRequestText: {
        fontSize: 18,
        alignSelf: 'center',
        color: '#ffffff',
        fontWeight: 'bold',
        paddingVertical: 9
    },
})
export default styles;