import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../screens/login/login';
import Signup from '../screens/signup/signup';
import Home from '../screens/Home/home';
import ContactNumber from '../screens/contactNumber/contactNumber';
import OTPVerification from '../screens/OTPVerification/otpVerfication';
import GiverBloodInfo from '../screens/giverBloodInfo/giverBloodInfo';
import GiverInfo from '../screens/giverInfo/giverInfo';
import BeGiver from '../screens/becomeAGiver/beGiver';
import Tutorial from '../screens/tutorial/tutorial';
import Seeker from '../screens/seeker/seeker';
import SeekBlood from '../screens/seekBlood/seekBlood';
import SeekingBlood from '../screens/seekingBlood/seekingBlood';
import AvailableGivers from '../screens/availableGivers/availableGivers';
import ContactGivers from '../screens/contactGivers/contactGivers';
import { View, Text } from 'react-native';
import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import EditButton from '../components/headerEditButton/headerEditButton';

const MainStackNavigtor = createStackNavigator({
    home: {
        screen: Home,
        path: 'home',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: false,
        })
    },
    giverInfo: {
        screen: GiverInfo,
        path: 'giverInfo',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    },
    giverBloodInfo: {
        screen: GiverBloodInfo,
        path: 'giverBloodInfo',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    },
    beGiver: {
        screen: BeGiver,
        path: 'beGiver',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    },
    tutorial: {
        screen: Tutorial,
        path: 'tutorial',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: false,
        })
    },
    seeker: {
        screen: Seeker,
        path: 'seeker',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    },
    seekBlood: {
        screen: SeekBlood,
        path: 'seekBlood',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    },
    seekingBlood: {
        screen: SeekingBlood,
        path: 'seekingBlood',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: '#ff1654', elevation: 0 },
            headerTintColor: 'white'
        })
    },
    availableGivers: {
        screen: AvailableGivers,
        path: 'availableGivers',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: false,
        })
    },
    contactGivers: {
        screen: ContactGivers,
        path: 'contactGivers',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: 'BLOOD REQUEST',
            headerStyle: { backgroundColor: 'white', elevation: 0 },
            headerTitleAlign: 'center',
            headerTitleAllowFontScaling: false,
            headerTitleStyle: { fontWeight: 'bold', fontSize: 15 },
            headerRight: <EditButton />
        })
    }
}, {
    initialRouteName: 'seekBlood'
})

const LoginStackNavigator = createStackNavigator({
    login: {
        screen: Login,
        path: 'login',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: false
        })
    },
    signup: {
        screen: Signup,
        path: 'signup',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: false
        })
    },
    contactNumber: {
        screen: ContactNumber,
        path: 'contactNumber',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    },
    otpVerification: {
        screen: OTPVerification,
        path: 'otpVerification',
        navigationOptions: () => ({
            headerMode: 'none',
            headerShown: true,
            headerTitle: '',
            headerStyle: { backgroundColor: 'white', elevation: 0 }
        })
    }
})

const AppNavigator = createSwitchNavigator({
    homeScreen: { screen: MainStackNavigtor },
    loginScreen: { screen: LoginStackNavigator },
})

export default AppNavigator;
