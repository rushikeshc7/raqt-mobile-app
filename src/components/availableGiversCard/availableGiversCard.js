import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import { Card } from 'react-native-shadow-cards';
import styles from './styles';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { Avatar } from 'react-native-elements';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import IconMaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from 'react-navigation-hooks';


const AvailableGiversCard = (props) => {

    const [isLoading, isLoadingChange] = useState(true);
    const { navigate } = useNavigation();

    const onEdit = () => {
        navigate('contactGivers');
    }

    useEffect(() => {
        setTimeout(() => isLoadingChange(false), 1500)
    }, [])

    return (
        <Card style={styles.cardView}>
            {
                isLoading
                    ?
                    <SkeletonPlaceholder>
                        <View style={styles.cardView}>
                            <View style={styles.avatarView}>
                                <View style={{ borderRadius: 50 / 2, height: 50, width: 50, marginHorizontal: 15, marginTop: 10 }}></View>
                            </View>
                            <View style={styles.infoView}>
                                <View style={{ marginTop: 15, marginLeft: 20, height: 16, width: 100 }}></View>
                                <View style={{ height: 16, marginTop: 8, marginLeft: 20, width: 160 }}></View>
                                <View style={{ height: 16, marginTop: 8, marginLeft: 20, width: 160 }}></View>
                            </View>

                            <View style={styles.iconView}>
                                <View style={{ height: 10, width: 10, marginTop: 5 }}></View>
                            </View>
                        </View>
                    </SkeletonPlaceholder>
                    :
                    <TouchableOpacity style={styles.cardView} onPress={() => onEdit()}>
                        <View style={styles.avatarView}>
                            {
                                props.cardData.bloodDonated === 'New'
                                    ?
                                    <Avatar
                                        rounded
                                        source={{ uri: props.cardData.uri }}
                                        size='medium'
                                        containerStyle={styles.avatar}
                                    />
                                    :
                                    <View style={{ position: 'absolute' }}>
                                        <Avatar
                                            rounded
                                            source={{ uri: props.cardData.uri }}
                                            size='medium'
                                            containerStyle={styles.avatarStyle}
                                        />
                                        <IconMaterialIcons name='stars' color='#ff1654' size={18} style={styles.starIcon} />
                                    </View>
                            }
                        </View>
                        <View style={styles.infoView}>
                            <Text style={styles.nameText}>{props.cardData.name}</Text>
                            <Text style={styles.locationText}>{props.cardData.location}</Text>
                            {
                                props.cardData.bloodDonated === 'New'
                                    ?
                                    <Text style={styles.newBloodDonatedText}>Newbie Giver</Text>
                                    :
                                    <Text style={styles.bloodDonatedText}>{props.cardData.bloodDonated} mililitres donated</Text>
                            }
                        </View>

                        <View style={styles.iconView}>
                            <IconFontAwesome name='caret-right' color='#989898' size={20} />
                        </View>
                    </TouchableOpacity>
            }
        </Card>

    )
}

export default AvailableGiversCard;