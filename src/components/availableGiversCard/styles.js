import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    cardView: {
        height: 'auto',
        width: '100%',
        borderRadius: 9,
        marginBottom: 15,
        flexDirection: 'row'
    },
    avatarView: {
        alignSelf: 'center',
        width: '20%'
    },
    avatar: {
        alignSelf: 'center',
        marginLeft: 16,
        marginTop: 10,
        borderWidth: 0.7,
        borderColor: '#73c2fb'
    },
    avatarStyle: {
        zIndex: 0,
        marginTop: -20,
        borderWidth: 0.7,
        marginHorizontal: 13,
        borderColor: '#73c2fb'
    },
    starIcon: {
        zIndex: 1,
        alignSelf: 'flex-end',
        marginTop: -15,
        marginRight: 12,
        backgroundColor: 'white',
        borderRadius: 18 / 2,
    },
    infoView: {
        width: '65%'
    },
    nameText: {
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 10,
        marginLeft: 20
    },
    locationText: {
        fontSize: 16,
        marginTop: 1,
        marginLeft: 20,
        fontStyle: 'italic',
        color: "#989898"
    },
    newBloodDonatedText: {
        color: '#888888',
        fontSize: 16,
        fontStyle: 'italic',
        marginTop: 5,
        marginLeft: 20,
        fontWeight: 'bold',
    },
    bloodDonatedText: {
        color: '#ff1654',
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 5,
        marginLeft: 20
    },
    iconView: {
        width: '15%',
        alignSelf: 'center',
        marginHorizontal: 10
    }
})

export default styles;