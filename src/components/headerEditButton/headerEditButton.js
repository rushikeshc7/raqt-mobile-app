import React, { Component } from "react";
import IconSimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import { Text, View } from 'react-native';


const EditButton = () => {

    return (
        <View>
            <IconSimpleLine name='pencil' size={16} style={{ marginRight: 18 }} />
            <Text style={{ fontSize: 10, fontWeight: 'bold' }}>EDIT</Text>
        </View>
    )

}
export default EditButton;