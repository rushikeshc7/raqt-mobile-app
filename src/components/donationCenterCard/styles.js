import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    cardView: {
        height: 'auto',
        width: '100%',
        borderRadius: 9,
        marginBottom: 10
    },
    addressType: {
        color: '#ff1654',
        fontSize: 12,
        fontWeight: 'bold',
        marginTop: 10,
        marginLeft: 20
    },
    cityText: {
        fontSize: 16,
        fontWeight: 'bold',
        marginTop: 5,
        marginLeft: 20
    },
    locationText: {
        fontSize: 16,
        marginTop: 1,
        marginLeft: 20,
        fontStyle: 'italic',
        color: "#989898"
    },
    deleteEditView: {
        flexDirection: 'row',
        marginHorizontal: 10,
        marginVertical: 10,
        justifyContent: 'space-between'
    },
    deleteView: {
        borderRadius: 14,
        borderColor: '#78d9cb',
        borderWidth: 0.7,
    },
    deleteText: {
        fontSize: 12,
        color: "#aaa",
        fontWeight: 'bold',
        alignSelf: 'center',
        marginVertical: 4,
        marginHorizontal: 14
    },
    editView: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        marginTop: 2
    },
    editText: {
        color: '#989898',
        fontSize: 14,
        fontWeight: 'bold'
    },
    iconView: {
        marginLeft: 7
    }
})

export default styles;