import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from "react-native";
import { Card } from 'react-native-shadow-cards';
import styles from './styles';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";

const DonationCenterCard = (props) => {

    const [isLoading, isLoadingChange] = useState(true);

    const onEdit = () => {

    }

    const onDelete = () => {

    }

    useEffect(() => {
        setTimeout(() => isLoadingChange(false), 1500)
    },[])

    return (
        <Card style={styles.cardView}>
            {
                isLoading
                    ?
                    <SkeletonPlaceholder>
                        <View style={{ marginTop: 15, marginLeft: 20, height: 12, width: 50 }}></View>
                        <View style={{ height: 16, marginTop: 8, marginLeft: 20, width: 200 }}></View>
                        <View style={{ height: 16, marginTop: 8, marginLeft: 20, width: 180 }}></View>
                        <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 10, justifyContent: 'space-between' }}>
                            <View style={{ borderRadius: 14, height: 23, width: 70, marginLeft: 5 }}></View>
                            <View style={{ height: 14, width: 40, marginTop: 4 }}></View>
                        </View>
                    </SkeletonPlaceholder>
                    :
                    <View>
                        <Text style={styles.addressType}>{props.cardData.addressType}</Text>
                        <Text style={styles.cityText}>{props.cardData.city}</Text>
                        <Text style={styles.locationText}>{props.cardData.location}</Text>
                        <View style={styles.deleteEditView}>
                            <TouchableOpacity style={styles.deleteView} onPress={() => onDelete()}>
                                <Text style={styles.deleteText}>DELETE</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.editView} onPress={() => onEdit()}>
                                <Text style={styles.editText}>EDIT</Text>
                                <IconFontAwesome name='caret-right' color='#989898' size={20} style={styles.iconView} />
                            </TouchableOpacity>
                        </View>
                    </View>
            }
        </Card>

    )
}

export default DonationCenterCard;