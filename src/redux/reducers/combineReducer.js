import { combineReducers } from "redux";
import login from '../reducers/login';
import seekingBlood from '../reducers/seekingBlood';
import seekBlood from '../reducers/seekBlood'


export default combineReducers({
    login,
    seekingBlood,
    seekBlood
})