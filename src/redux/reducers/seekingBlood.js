import { SET_BLOOD_REQUEST_DATE } from "../actionTypes/actionTypes";

const initialState = {
    dateOfRequest: ''
}

function seekingBlood(state = initialState, action) {
    switch (action.type) {
        case SET_BLOOD_REQUEST_DATE:
            return {
                ...state,
                dateOfRequest: action.bloodRequestedDate
            }
        default:
            return state
    }

}
export default seekingBlood;