import { SET_PHONE_NUMBER } from "../actionTypes/actionTypes";

const initialState = {
    phoneNumber: ''
}

function login(state = initialState, action) {
    switch (action.type) {
        case SET_PHONE_NUMBER:
            return {
                ...state,
                phoneNumber: action.phoneNumber
            }
        default:
            return state;
    }
}
export default login;