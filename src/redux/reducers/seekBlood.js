import { SET_SEEK_BLOOD_GROUP, SET_ADMITTED_HOSPITAL, SET_REQUIRED_DATE_OF_BLOOD, SET_REQUIRED_BLOOD_UNITS } from "../actionTypes/actionTypes";

const initialState = {
    selectedBloodGroup: 'A+',
    admittedHospital: '',
    requiredDateOfBlood: '',
    requiredUnitsOfBlood: 1
}

function seekBlood(state = initialState, action) {
    switch (action.type) {
        case SET_SEEK_BLOOD_GROUP:
            return {
                ...state,
                selectedBloodGroup: action.bloodGroupSelected
            }
        case SET_ADMITTED_HOSPITAL:
            return {
                ...state,
                admittedHospital: action.admittedHospital
            }
        case SET_REQUIRED_DATE_OF_BLOOD:
            return {
                ...state,
                requiredDateOfBlood: action.requiredDateOfBlood
            }
        case SET_REQUIRED_BLOOD_UNITS:
            return {
                ...state,
                requiredUnitsOfBlood: action.requiredUnitsOfBlood
            }
        default:
            return state
    }

}
export default seekBlood;