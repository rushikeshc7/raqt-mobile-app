export const SET_PHONE_NUMBER = 'SET_PHONE_NUMBER';
export const SET_BLOOD_REQUEST_DATE = 'SET_BLOOD_REQUEST_DATE';
export const SET_SEEK_BLOOD_GROUP = 'SET_SEEK_BLOOD_GROUP';
export const SET_ADMITTED_HOSPITAL = 'SET_ADMITTED_HOSPITAL';
export const SET_REQUIRED_DATE_OF_BLOOD = 'SET_REQUIRED_DATE_OF_BLOOD';
export const SET_REQUIRED_BLOOD_UNITS = 'SET_REQUIRED_BLOOD_UNITS';  