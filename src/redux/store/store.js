import combineReducer from "../reducers/combineReducer";
import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from 'redux-saga';
import thunk from "redux-thunk";
import logger from "redux-logger";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    combineReducer,
    applyMiddleware(thunk, logger, sagaMiddleware)
)
// sagaMiddleware.run();
export default store;